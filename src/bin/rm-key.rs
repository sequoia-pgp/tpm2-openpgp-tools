use std::str::FromStr;
use std::io::ErrorKind;
use tss_esapi::Tcti;
use tss_esapi::{
    attributes::object::ObjectAttributesBuilder,
    attributes::SessionAttributesBuilder,
    constants::{
        session_type::SessionType,
    },
    handles::{PersistentTpmHandle, TpmHandle},
    interface_types::{
        algorithm::HashingAlgorithm,
        algorithm::PublicAlgorithm,
        dynamic_handles::Persistent,
        key_bits::RsaKeyBits,
        resource_handles::Provision,
    },
    structures::{
        Public,
        PublicBuilder,
        PublicRsaParametersBuilder,
        RsaExponent,
        SymmetricDefinition,
        SymmetricDefinitionObject,
    },
    Context,
};

pub fn create_restricted_decryption_rsa_public(
    symmetric: SymmetricDefinitionObject,
    key_bits: RsaKeyBits,
    pub_exponent: RsaExponent,
) -> tss_esapi::Result<Public> {
    let rsa_parms = PublicRsaParametersBuilder::new_restricted_decryption_key(
        symmetric, key_bits, pub_exponent)
        .build()?;

    let object_attributes = ObjectAttributesBuilder::new()
        .with_fixed_tpm(true)
        .with_fixed_parent(true)
        .with_sensitive_data_origin(true)
        .with_user_with_auth(true)
        .with_decrypt(true)
        .with_sign_encrypt(false)
        .with_restricted(true)
        .build()?;

    PublicBuilder::new()
        .with_public_algorithm(PublicAlgorithm::Rsa)
        .with_name_hashing_algorithm(HashingAlgorithm::Sha256)
        .with_object_attributes(object_attributes)
        .with_rsa_parameters(rsa_parms)
        .build()
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("1");
    let tcti_cfg: Option<String> = Some("device:/dev/tpmrm0".into());
    //tcti.zeroize();

    let tcti = Tcti::from_str(tcti_cfg.as_ref().ok_or_else(|| {
        std::io::Error::new(ErrorKind::InvalidData, "TCTI configuration missing")
    })?)
    .map_err(|e| {
        println!("xx");
        std::io::Error::new(
            ErrorKind::InvalidData,
            format!("XInvalid TCTI configuration string: {:#?}", e),
        )
    })?;

    let mut context = Context::new(tcti)?;

    //context.

    let session = context.start_auth_session(
        None,
        None,
        None,
        SessionType::Hmac,
        SymmetricDefinition::AES_256_CFB,
        HashingAlgorithm::Sha256,
    )?;
    let (session_attrs, session_attrs_mask) = SessionAttributesBuilder::new()
        .with_decrypt(true)
        .with_encrypt(true)
        .build();
    context.tr_sess_set_attributes(
        session.unwrap(), session_attrs, session_attrs_mask)?;
    context.set_sessions((session, None, None));

    println!("2");
    //context.ev
    // println!("Vendor = {}", tss_esapi::utils::get_tpm_vendor(&mut context)?);

    // GENERATE AND PERSIST IN TPM
    let persistent_tpm_handle =
        PersistentTpmHandle::new(u32::from_be_bytes([0x81, 0x00, 0x00, 0x26]))?;

    // evict control

    // Create persistent TPM handle with
    // Create interface type Persistent by using the handle.
    let persistent = Persistent::Persistent(persistent_tpm_handle);

    let handle = context.execute_without_session(|ctx| {
        let handle = ctx
            .tr_from_tpm_public(TpmHandle::Persistent(persistent_tpm_handle))
            .expect("Need handle");
        return handle;
    });

    context.evict_control(Provision::Owner, handle.into(), persistent)?;
    //context.rsa_decrypt(key_handle, cipher_text, in_scheme, label)
    println!("Persistent = {:?}", persistent);

    Ok(())
}
