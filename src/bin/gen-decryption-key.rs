use std::str::FromStr;
use std::{convert::TryFrom, convert::TryInto, io::ErrorKind};
use tss_esapi::Tcti;
use tss_esapi::{
    attributes::SessionAttributesBuilder,
    attributes::object::ObjectAttributesBuilder,
    constants::{
        session_type::SessionType,
    },
    handles::PersistentTpmHandle,
    interface_types::{
        algorithm::HashingAlgorithm,
        algorithm::PublicAlgorithm,
        dynamic_handles::Persistent,
        key_bits::RsaKeyBits,
        resource_handles::{Hierarchy, Provision},
    },
    structures::{
        Auth,
        Public,
        PublicBuilder,
        PublicRsaParametersBuilder,
        RsaExponent,
        RsaScheme,
        SymmetricDefinition,
    },
    Context,
};

pub fn create_restricted_decryption_rsa_public(
    key_bits: RsaKeyBits,
    pub_exponent: RsaExponent,
) -> tss_esapi::Result<Public> {
    let rsa_parms = PublicRsaParametersBuilder::new()
        // XXX: For 0.4, this was Some(AsymSchemeUnion::AnySig(None)).
        // Is this right?
        // https://docs.rs/tss-esapi/4.0.10-alpha.2/tss_esapi/utils/enum.AsymSchemeUnion.html
        // https://docs.rs/tss-esapi/7.5.1/tss_esapi/structures/enum.RsaScheme.html
        .with_scheme(RsaScheme::Null)
        .with_key_bits(key_bits)
        .with_exponent(pub_exponent)
        .with_is_signing_key(true)
        .with_is_decryption_key(true)
        // XXX: The function is named "restricted", but we pass false
        // (as well as below).  (This was done in the original
        // version.)  Check.
        .with_restricted(false)
        .build()?;

    let object_attributes = ObjectAttributesBuilder::new()
        .with_fixed_tpm(true)
        .with_fixed_parent(true)
        .with_sensitive_data_origin(true)
        .with_user_with_auth(true)
        .with_decrypt(true)
        .with_sign_encrypt(true)
        .with_restricted(false)
        .build()?;

    PublicBuilder::new()
        .with_public_algorithm(PublicAlgorithm::Rsa)
        .with_name_hashing_algorithm(HashingAlgorithm::Sha256)
        .with_object_attributes(object_attributes)
        .with_rsa_parameters(rsa_parms)
        .build()
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("1");
    let tcti_cfg: Option<String> = Some("device:/dev/tpmrm0".into());
    //tcti.zeroize();

    let tcti = Tcti::from_str(tcti_cfg.as_ref().ok_or_else(|| {
        std::io::Error::new(ErrorKind::InvalidData, "TCTI configuration missing")
    })?)
    .map_err(|e| {
        println!("xx");
        std::io::Error::new(
            ErrorKind::InvalidData,
            format!("XInvalid TCTI configuration string: {:#?}", e),
        )
    })?;

    let mut context = Context::new(tcti)?;

    //context.load(parent_handle, private, public)
    //context.

    let session = context.start_auth_session(
        None,
        None,
        None,
        SessionType::Hmac,
        SymmetricDefinition::AES_256_CFB,
        HashingAlgorithm::Sha256,
    )?;
    let (session_attrs, session_attrs_mask) = SessionAttributesBuilder::new()
        .with_decrypt(true)
        .with_encrypt(true)
        .build();
    context.tr_sess_set_attributes(
        session.unwrap(), session_attrs, session_attrs_mask)?;
    context.set_sessions((session, None, None));

    println!("2");
    //context.ev
    // println!("Vendor = {}", tss_esapi::utils::get_tpm_vendor(&mut context)?);

    // GENERATE AND PERSIST IN TPM
    let persistent_tpm_handle =
        PersistentTpmHandle::new(u32::from_be_bytes([0x81, 0x00, 0x00, 0x26]))?;

    //let random_digest = context.get_random(16).unwrap();
    let key_auth = Auth::try_from(vec![0x01, 0x02, 0x07])?;
    let pk = context.create_primary(
        Hierarchy::Owner,
        create_restricted_decryption_rsa_public(
            RsaKeyBits::Rsa2048, RsaExponent::ZERO_EXPONENT)?.try_into()?,
        Some(key_auth),
        None,
        None,
        None,
    )?;
    println!("Handle = 0x{:X}", pk.key_handle.value());

    // evict control
    // Create persistent TPM handle with
    // Create interface type Persistent by using the handle.
    let persistent = Persistent::Persistent(persistent_tpm_handle);

    context.evict_control(Provision::Owner, pk.key_handle.into(), persistent)?;

    //let pubkey = context.read_public(pk.key_handle.into())?;

    Ok(())
}
