use std::{
    convert::TryFrom,
    io::{self, ErrorKind},
    str::FromStr,
    time::SystemTime,
};

use sequoia_openpgp as openpgp;

use openpgp::{
    crypto::SessionKey,
    packet::{
        key::{PrimaryRole, PublicParts},
        prelude::*,
    },
    parse::{
        stream::{DecryptionHelper, DecryptorBuilder, MessageStructure, VerificationHelper},
        Parse,
    },
    policy::{NullPolicy, Policy},
    types::SymmetricAlgorithm,
};
use tss_esapi::{
    attributes::SessionAttributesBuilder,
    constants::{
        session_type::SessionType,
    },
    handles::{KeyHandle, PersistentTpmHandle, TpmHandle},
    interface_types::algorithm::HashingAlgorithm,
    structures::{Auth, Data, Public, PublicKeyRsa, SymmetricDefinition, RsaDecryptionScheme},
    Context, Tcti,
};

fn main() -> openpgp::Result<()> {
    eprintln!("1");
    let tcti_cfg: Option<String> = Some("device:/dev/tpmrm0".into());
    //tcti.zeroize();

    let tcti = Tcti::from_str(tcti_cfg.as_ref().ok_or_else(|| {
        std::io::Error::new(ErrorKind::InvalidData, "TCTI configuration missing")
    })?)
    .map_err(|e| {
        eprintln!("xx");
        std::io::Error::new(
            ErrorKind::InvalidData,
            format!("XInvalid TCTI configuration string: {:#?}", e),
        )
    })?;

    let mut context = Context::new(tcti)?;

    //context.

    let session = context.start_auth_session(
        None,
        None,
        None,
        SessionType::Hmac,
        SymmetricDefinition::AES_256_CFB,
        HashingAlgorithm::Sha256,
    )?;
    let (session_attrs, session_attrs_mask) = SessionAttributesBuilder::new()
        .with_decrypt(true)
        .with_encrypt(true)
        .build();
    context.tr_sess_set_attributes(
        session.unwrap(), session_attrs, session_attrs_mask)?;
    context.set_sessions((session, None, None));

    eprintln!("2");
    //context.ev
    // println!("Vendor = {}", tss_esapi::utils::get_tpm_vendor(&mut context)?);

    // GENERATE AND PERSIST IN TPM
    let persistent_tpm_handle =
        PersistentTpmHandle::new(u32::from_be_bytes([0x81, 0x00, 0x00, 0x26]))?;

    eprintln!("3");
    let handle = context.execute_without_session(|ctx| {
        ctx.tr_from_tpm_public(TpmHandle::Persistent(persistent_tpm_handle))
            .expect("Need handle")
    });

    eprintln!("4");

    let key_handle: KeyHandle = handle.into();

    eprintln!("handle: = {:?}", key_handle);
    let (key_pub, _, _) = context.read_public(key_handle)?;

    if let Public::Rsa {
        object_attributes: _object_attributes,
        name_hashing_algorithm: _name_hashing_algorithm,
        auth_policy: _auth_policy,
        parameters: _parameters,
        unique
    } = key_pub {
        let key: Vec<u8> = unique.value().to_vec();
        // XXX(N): key.truncate(pub_key.size.try_into().unwrap()); // should not fail on supported targets
        eprintln!("Key = {:X?}", key);

        let key4: Key<PublicParts, PrimaryRole> =
            Key4::import_public_rsa(&[1, 0, 1], &key, SystemTime::UNIX_EPOCH)?.into();
        eprintln!("Key4 = {:X?}", key4);

        decrypt(&NullPolicy::new(), &mut context, key_handle)?;
    } else {
        eprintln!("Not RSA?");
    }

    Ok(())
}

/// Decrypts the given message.
fn decrypt(
    policy: &dyn Policy,
    context: &mut Context,
    key_handle: KeyHandle,
) -> openpgp::Result<()> {
    // Make a helper that that feeds the recipient's secret key to the
    // decryptor.
    let helper = Helper {
        context,
        key_handle,
    };

    // Now, create a decryptor with a helper using the given Certs.
    let stdin = std::io::stdin();
    let mut decryptor = DecryptorBuilder::from_reader(stdin)?.with_policy(policy, None, helper)?;

    // Decrypt the data.
    let mut stdout = std::io::stdout();
    io::copy(&mut decryptor, &mut stdout)?;

    Ok(())
}

struct Helper<'a> {
    context: &'a mut Context,
    key_handle: KeyHandle,
}

impl<'a> VerificationHelper for Helper<'a> {
    fn get_certs(&mut self, _ids: &[openpgp::KeyHandle]) -> openpgp::Result<Vec<openpgp::Cert>> {
        // Return public keys for signature verification here.
        Ok(Vec::new())
    }

    fn check(&mut self, _structure: MessageStructure) -> openpgp::Result<()> {
        // Implement your signature verification policy here.
        Ok(())
    }
}

impl<'a> DecryptionHelper for Helper<'a> {
    fn decrypt<D>(
        &mut self,
        pkesks: &[openpgp::packet::PKESK],
        _skesks: &[openpgp::packet::SKESK],
        sym_algo: Option<SymmetricAlgorithm>,
        mut decrypt: D,
    ) -> openpgp::Result<Option<openpgp::Fingerprint>>
    where
        D: FnMut(SymmetricAlgorithm, &SessionKey) -> bool,
    {
        let mut pair = TpmKeyPair {
            context: self.context,
            key_handle: self.key_handle,
        };
        //key.into_keypair().unwrap();

        pkesks[0]
            .decrypt(&mut pair, sym_algo)
            .map(|(algo, session_key)| decrypt(algo, &session_key));

        // XXX: In production code, return the Fingerprint of the
        // recipient's Cert here
        Ok(None)
    }
}

struct TpmKeyPair<'a> {
    context: &'a mut Context,
    key_handle: KeyHandle,
}

impl openpgp::crypto::Decryptor for TpmKeyPair<'_> {
    fn public(&self) -> &Key<PublicParts, key::UnspecifiedRole> {
        //self.key
        panic!("No biggie");
    }

    fn decrypt(
        &mut self,
        ciphertext: &openpgp::crypto::mpi::Ciphertext,
        _plaintext_len: Option<usize>,
    ) -> openpgp::Result<SessionKey> {
        use openpgp::crypto::mpi::Ciphertext::*;
        if let RSA { c } = ciphertext {
            let bytes = c.value();
            let cipher_text: PublicKeyRsa = PublicKeyRsa::try_from(bytes)?;
            self.context.tr_set_auth(
                self.key_handle.into(),
                Auth::try_from(vec![0x01, 0x02, 0x07])?,
            )?;
            let decrypted = self.context.rsa_decrypt(
                self.key_handle,
                cipher_text,
                RsaDecryptionScheme::RsaEs,
                Data::default(),
            )?;
            eprintln!("Decrypted session key = {:X?}", decrypted);
            Ok(decrypted.as_slice().into())
        } else {
            Err(openpgp::Error::InvalidOperation(format!(
                "Don't know how to handle non-RSA things."
            ))
            .into())
        }
    }
}
