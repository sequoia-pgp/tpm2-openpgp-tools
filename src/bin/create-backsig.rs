use openpgp::{
    packet::{key::UnspecifiedRole, signature},
    types::{HashAlgorithm, SignatureType},
    Error,
};
use openpgp::{
    packet::{
        key::{PublicParts, SubordinateRole},
        prelude::Key4,
        Key,
    },
    parse::Parse,
    serialize::Marshal,
    Cert,
};
use sequoia_openpgp as openpgp;
use std::{convert::TryFrom, fs::File, path::PathBuf, str::FromStr};
use std::{convert::TryInto, io, num::ParseIntError, time::SystemTime};
use structopt::StructOpt;
use tss_esapi::tss2_esys::{TPMT_SIG_SCHEME, TPMT_TK_HASHCHECK};
use tss_esapi::{
    constants::tss::{TPM2_ALG_NULL, TPM2_RH_NULL, TPM2_ST_HASHCHECK},
    structures::Auth,
    structures::SymmetricDefinition,
};
use tss_esapi::{
    attributes::SessionAttributesBuilder,
    constants::{
        session_type::SessionType,
    },
    handles::PersistentTpmHandle,
    interface_types::algorithm::HashingAlgorithm,
    Context,
};
use tss_esapi::{
    handles::{KeyHandle, TpmHandle},
    structures::Digest as TpmDigest,
    structures::Public,
    structures::Signature,
    Tcti,
};

fn parse_hex(src: &str) -> Result<u32, ParseIntError> {
    u32::from_str_radix(src, 16)
}

#[derive(StructOpt, Debug)]
#[structopt(name = "create-backsig")]
struct Opt {
    /// TCTI configuration. It can be used to run the program against TPM simulator.
    /// By default it uses hardware TPM. Depending on the device permissions it may require
    /// running with elevated permissions.
    #[structopt(short, long, default_value = "device:/dev/tpmrm0")]
    tcti: String,

    /// Authentication file. It's like a PIN for accessing the key. You can use `head -c 3 /dev/urandom`
    /// to generate this file. Do not lose it as it's required to use the key!
    #[structopt(short, long, parse(from_os_str))]
    auth_file: PathBuf,

    /// Handle for the persisted object, for example: 81000016.
    /// Use `tpm2_getcap handles-persistent` and use a value that is not present in the output.
    #[structopt(short, long, parse(try_from_str = parse_hex))]
    handle: u32,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let stdin = io::stdin();
    let cert = Cert::from_reader(stdin)?;

    let tcti = Tcti::from_str(&opt.tcti)?;

    let mut context = Context::new(tcti)?;

    let session = context.start_auth_session(
        None,
        None,
        None,
        SessionType::Hmac,
        SymmetricDefinition::AES_256_CFB,
        HashingAlgorithm::Sha256,
    )?;

    let (session_attrs, session_attrs_mask) = SessionAttributesBuilder::new()
        .with_encrypt(true)
        .build();
    context.tr_sess_set_attributes(
        session.unwrap(), session_attrs, session_attrs_mask)?;
    context.set_sessions((session, None, None));

    let persistent_tpm_handle = PersistentTpmHandle::new(opt.handle)?;

    let handle = context.execute_without_session(|ctx| {
        ctx.tr_from_tpm_public(TpmHandle::Persistent(persistent_tpm_handle))
            .expect("Need handle")
    });

    let key_auth = {
        use std::io::prelude::*;
        let mut f = File::open(opt.auth_file)?;
        let mut v = Vec::new();
        f.read(&mut v)?;
        Auth::try_from(v)?
    };

    let key_handle: KeyHandle = handle.into();
    let (key_pub, _, _) = context.read_public(key_handle)?;

    if let Public::Rsa {
        object_attributes: _object_attributes,
        name_hashing_algorithm: _name_hashing_algorithm,
        auth_policy: _auth_policy,
        parameters: _parameters,
        unique
    } = key_pub {
        let key: Vec<u8> = unique.value().to_vec();
        // XXX(N): key.truncate(pub_key.size.try_into().unwrap()); // should not fail on supported targets

        let key4: Key<PublicParts, SubordinateRole> =
            Key4::import_public_rsa(&[1, 0, 1], &key, SystemTime::UNIX_EPOCH)?.into();

        let mut subkey_signer = TpmKeyPair {
            context: &mut context,
            key_handle: key_handle,
            public: &key4.role_as_unspecified(),
            auth: &key_auth,
        };

        let backsig = signature::SignatureBuilder::new(SignatureType::PrimaryKeyBinding)
            .set_signature_creation_time(SystemTime::now())?
            .set_hash_algo(HashAlgorithm::SHA256)
            .sign_primary_key_binding(&mut subkey_signer, &cert.primary_key(), &key4)?;

        let mut stdout = io::stdout();
        backsig.serialize(&mut stdout)?;

        Ok(())
    } else {
        Err(Error::InvalidOperation(format!("Don't know how to handle non-RSA things.")).into())
    }
}

struct TpmKeyPair<'a> {
    context: &'a mut Context,
    key_handle: KeyHandle,
    public: &'a Key<PublicParts, UnspecifiedRole>,
    auth: &'a Auth,
}

impl openpgp::crypto::Signer for TpmKeyPair<'_> {
    fn public(&self) -> &Key<PublicParts, UnspecifiedRole> {
        self.public
    }

    fn sign(
        &mut self,
        hash_algo: HashAlgorithm,
        digest: &[u8],
    ) -> openpgp::Result<openpgp::crypto::mpi::Signature> {
        assert_eq!(hash_algo, HashAlgorithm::SHA256);
        let scheme = TPMT_SIG_SCHEME {
            scheme: TPM2_ALG_NULL,
            details: Default::default(),
        };
        let validation = TPMT_TK_HASHCHECK {
            tag: TPM2_ST_HASHCHECK,
            hierarchy: TPM2_RH_NULL,
            digest: Default::default(),
        }
        .try_into()?;

        let digest = TpmDigest::try_from(digest.to_vec())?;

        self.context
            .tr_set_auth(self.key_handle.into(), self.auth.clone())?;
        let signature = self
            .context
            .sign(self.key_handle, digest, scheme.try_into()?, validation)?;
        match signature {
            Signature::RsaSsa(sig) | Signature::RsaPss(sig) => {
                let sig = sig.signature().value().to_vec();
                Ok(openpgp::crypto::mpi::Signature::RSA { s: sig.into() })
            }
            _ => {
                Err(openpgp::Error::InvalidOperation(format!(
                    "Don't know how to handle non-RSA things."
                )).into())
            }
        }
    }
}
