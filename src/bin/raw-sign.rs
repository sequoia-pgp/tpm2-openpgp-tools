use std::{
    convert::{TryFrom, TryInto},
    io::ErrorKind,
    str::FromStr,
};

use sequoia_openpgp as openpgp;

use tss_esapi::{
    Context, Tcti,
    attributes::SessionAttributesBuilder,
    constants::{
        session_type::SessionType,
    },
    handles::{KeyHandle, PersistentTpmHandle, TpmHandle},
    interface_types::algorithm::HashingAlgorithm,
    structures::{Auth, Digest as TpmDigest, SymmetricDefinition}
};
use tss_esapi::tss2_esys::{TPMT_SIG_SCHEME, TPMT_TK_HASHCHECK};
use tss_esapi::constants::tss::{TPM2_ALG_NULL, TPM2_ST_HASHCHECK, TPM2_RH_NULL};


const HASH: [u8; 64] = [
    0x69, 0x3E, 0xDB, 0x1B, 0x22, 0x79, 0x03, 0xF4, 0xC0, 0xBF, 0xD6, 0x91, 0x76, 0x37, 0x84, 0x69,
    0x3E, 0xDB, 0x1B, 0x22, 0x79, 0x03, 0xF4, 0xC0, 0xBF, 0xD6, 0x91, 0x76, 0x37, 0x84, 0xA2, 0x94,
    0x8E, 0x92, 0x50, 0x35, 0xC2, 0x8C, 0x5C, 0x3C, 0xCA, 0xFE, 0x18, 0xE8, 0x81, 0xA2, 0x94, 0x8E,
    0x92, 0x50, 0x35, 0xC2, 0x8C, 0x5C, 0x3C, 0xCA, 0xFE, 0x18, 0xE8, 0x81, 0x37, 0x78, 0x37, 0x78,
];

fn main() -> openpgp::Result<()> {
    eprintln!("1");
    let tcti_cfg: Option<String> = Some("device:/dev/tpmrm0".into());
    //tcti.zeroize();

    let tcti = Tcti::from_str(tcti_cfg.as_ref().ok_or_else(|| {
        std::io::Error::new(ErrorKind::InvalidData, "TCTI configuration missing")
    })?)
    .map_err(|e| {
        eprintln!("xx");
        std::io::Error::new(
            ErrorKind::InvalidData,
            format!("XInvalid TCTI configuration string: {:#?}", e),
        )
    })?;

    let mut context = Context::new(tcti)?;

    //context.

    let session = context.start_auth_session(
        None,
        None,
        None,
        SessionType::Hmac,
        SymmetricDefinition::AES_256_CFB,
        HashingAlgorithm::Sha256,
    )?;
    let (session_attrs, session_attrs_mask) = SessionAttributesBuilder::new()
        .with_decrypt(true)
        .with_encrypt(true)
        .build();
    context.tr_sess_set_attributes(
        session.unwrap(), session_attrs, session_attrs_mask)?;
    context.set_sessions((session, None, None));

    eprintln!("2");
    //context.ev
    // println!("Vendor = {}", tss_esapi::utils::get_tpm_vendor(&mut context)?);

    // GENERATE AND PERSIST IN TPM
    let persistent_tpm_handle =
        PersistentTpmHandle::new(u32::from_be_bytes([0x81, 0x00, 0x00, 0x16]))?;

    eprintln!("3");
    let handle = context.execute_without_session(|ctx| {
        ctx.tr_from_tpm_public(TpmHandle::Persistent(persistent_tpm_handle))
            .expect("Need handle")
    });

    eprintln!("4");

    let key_handle: KeyHandle = handle.into();

    eprintln!("handle: = {:?}", key_handle);

    let scheme = TPMT_SIG_SCHEME {
        scheme: TPM2_ALG_NULL,
        details: Default::default(),
    };
    let validation = TPMT_TK_HASHCHECK {
        tag: TPM2_ST_HASHCHECK,
        hierarchy: TPM2_RH_NULL,
        digest: Default::default(),
    }.try_into()?;

    let digest = TpmDigest::try_from(HASH[..32].to_vec())?;

    context.tr_set_auth(
        key_handle.into(),
        Auth::try_from(vec![0x01, 0x02, 0x07])?,
    )?;
    let signature = context.sign(key_handle, digest, scheme.try_into()?, validation)?;

    eprintln!(":: {:?}", signature);

    Ok(())
}
