use std::{
    convert::{TryFrom, TryInto},
    io::{ErrorKind, Write},
    str::FromStr,
    time::SystemTime,
};

use key::SubordinateRole;
use sequoia_openpgp as openpgp;

use openpgp::{cert::CertBuilder, packet::{
        key::{PublicParts, UnspecifiedRole},
        prelude::*,
    }, serialize::{Marshal, SerializeInto, stream::{Armorer, Encryptor2, LiteralWriter, Message, Recipient}}, types::{Features, HashAlgorithm, KeyFlags, SignatureType}};
use tss_esapi::tss2_esys::{TPMT_SIG_SCHEME, TPMT_TK_HASHCHECK};
use tss_esapi::constants::tss::{TPM2_ALG_NULL, TPM2_ST_HASHCHECK, TPM2_RH_NULL};
use tss_esapi::{
    attributes::SessionAttributesBuilder,
    constants::{
        session_type::SessionType,
    },
    handles::{KeyHandle, PersistentTpmHandle, TpmHandle},
    interface_types::algorithm::HashingAlgorithm,
    structures::{Auth, Digest as TpmDigest, Public, Signature, SymmetricDefinition},
    Context, Tcti,
};

fn main() -> openpgp::Result<()> {
    //Key4::import_public_rsa(e, n, ctime);

    eprintln!("1");
    let tcti_cfg: Option<String> = Some("device:/dev/tpmrm0".into());
    //tcti.zeroize();

    let tcti = Tcti::from_str(tcti_cfg.as_ref().ok_or_else(|| {
        std::io::Error::new(ErrorKind::InvalidData, "TCTI configuration missing")
    })?)
    .map_err(|e| {
        eprintln!("xx");
        std::io::Error::new(
            ErrorKind::InvalidData,
            format!("XInvalid TCTI configuration string: {:#?}", e),
        )
    })?;

    let mut context = Context::new(tcti)?;

    //context.

    let session = context.start_auth_session(
        None,
        None,
        None,
        SessionType::Hmac,
        SymmetricDefinition::AES_256_CFB,
        HashingAlgorithm::Sha256,
    )?;
    let (session_attrs, session_attrs_mask) = SessionAttributesBuilder::new()
        .with_decrypt(true)
        .with_encrypt(true)
        .build();
    context.tr_sess_set_attributes(
        session.unwrap(), session_attrs, session_attrs_mask)?;
    context.set_sessions((session, None, None));

    eprintln!("2");
    //context.ev
    // println!("Vendor = {}", tss_esapi::utils::get_tpm_vendor(&mut context)?);

    let cert = CertBuilder::new()
        .add_userid("tpm-test@metacode.biz")
        .generate()?
        .0;

    let c2 = cert.clone();
    let k1 = c2.keys().unencrypted_secret().nth(0).unwrap().key().clone();
    let mut signer = k1.into_keypair()?;

    // ENCRYPTION SUBKEY
    let decryption_builder = signature::SignatureBuilder::new(SignatureType::SubkeyBinding)
        .set_signature_creation_time(SystemTime::now())?
        // GnuPG wants at least a 512-bit hash for P521 keys.
        .set_hash_algo(HashAlgorithm::SHA512)
        .set_features(Features::sequoia())?
        .set_key_flags(
            KeyFlags::empty()
                .set_storage_encryption()
                .set_transport_encryption(),
        )?;

    let decryption_key =
        rsa_key_from_handle(&mut context, u32::from_be_bytes([0x81, 0x00, 0x00, 0x26]))?;
    let decryption_bsig = decryption_key.bind(&mut signer, &cert, decryption_builder)?;

    // SIGNING SUBKEY
    let signing_builder = signature::SignatureBuilder::new(SignatureType::SubkeyBinding)
        .set_signature_creation_time(SystemTime::now())?
        // TPM requires SHA256 for now.
        .set_hash_algo(HashAlgorithm::SHA256)
        .set_features(Features::sequoia())?
        .set_key_flags(KeyFlags::empty().set_signing())?;
    // this is signing-capable, we need a backsig

    let signing_key =
        rsa_key_from_handle(&mut context, u32::from_be_bytes([0x81, 0x00, 0x00, 0x16]))?;

    let signing_key_handle = context.execute_without_session(|ctx| {
        ctx.tr_from_tpm_public(TpmHandle::Persistent(PersistentTpmHandle::new(u32::from_be_bytes([0x81, 0x00, 0x00, 0x16])).unwrap()))
            .expect("Need handle")
    }).into();

    let mut subkey_signer = TpmKeyPair {
        context: &mut context,
        key_handle: signing_key_handle,
        public: &signing_key.role_as_unspecified(),
    };
    let backsig = signature::SignatureBuilder::new(SignatureType::PrimaryKeyBinding)
        .set_signature_creation_time(SystemTime::now())?
        .set_hash_algo(HashAlgorithm::SHA256)
        .sign_primary_key_binding(&mut subkey_signer, &cert.primary_key(), &signing_key)?;

    {
        let mut file = std::fs::File::create("back.sig")?;
        backsig.serialize(&mut file)?;
    }

    let signing_builder = signing_builder.set_embedded_signature(backsig)?;

    let signing_bsig = signing_key.bind(&mut signer, &cert, signing_builder)?;

    let cert = cert.insert_packets(vec![
        Packet::PublicSubkey(decryption_key.clone()),
        decryption_bsig.into(),
        Packet::PublicSubkey(signing_key.clone()),
        signing_bsig.into(),
    ])?;

    eprintln!("Cert = {:?}", cert);
    eprintln!(
        "Armored = \n{}",
        String::from_utf8_lossy(&cert.armored().to_vec()?)
    );

    let mut ciphertext = Vec::new();
    encrypt(&mut ciphertext, &"test this thing", &decryption_key)?;

    println!("{}", String::from_utf8_lossy(&ciphertext));

    Ok(())
}

fn rsa_key_from_handle(
    context: &mut Context,
    handle: u32,
) -> openpgp::Result<Key<PublicParts, SubordinateRole>> {
    let persistent_tpm_handle = PersistentTpmHandle::new(handle)?;

    let handle = context.execute_without_session(|ctx| {
        ctx.tr_from_tpm_public(TpmHandle::Persistent(persistent_tpm_handle))
            .expect("Need handle")
    });

    let key_handle: KeyHandle = handle.into();

    eprintln!("handle: = {:?}", key_handle);
    let (key_pub, _, _) = context.read_public(key_handle)?;

    if let Public::Rsa {
        object_attributes: _object_attributes,
        name_hashing_algorithm: _name_hashing_algorithm,
        auth_policy: _auth_policy,
        parameters: _parameters,
        unique
    } = key_pub {
        let key: Vec<u8> = unique.value().to_vec();
        // XXX(N): key.truncate(pub_key.size.try_into().unwrap()); // should not fail on supported targets

        eprintln!("Key = {:X?}", key);

        Ok(Key4::import_public_rsa(&[1, 0, 1], &key, SystemTime::UNIX_EPOCH)?.into())
    } else {
        Err(
            openpgp::Error::InvalidOperation(format!("Don't know how to handle non-RSA things."))
                .into(),
        )
    }
}

fn encrypt(
    sink: &mut (dyn Write + Send + Sync),
    plaintext: &str,
    key4: &Key<PublicParts, SubordinateRole>,
) -> openpgp::Result<()> {
    //let recipients =
    //    recipient.keys();//.with_policy(policy, None);//.supported().alive().revoked(false)
    //    ;//.for_transport_encryption();

    //println!("recipients: {}", recipients.count());
    //return Ok(());

    // Start streaming an OpenPGP message.
    let message = Message::new(sink);

    let message = Armorer::new(message).build()?;

    // We want to encrypt a literal data packet.
    let message = Encryptor2::for_recipients(message, vec![Recipient::from(key4)]).build()?;

    // Emit a literal data packet.
    let mut message = LiteralWriter::new(message).build()?;

    // Encrypt the data.
    message.write_all(plaintext.as_bytes())?;

    // Finalize the OpenPGP message to make sure that all data is
    // written.
    message.finalize()?;

    Ok(())
}

struct TpmKeyPair<'a> {
    context: &'a mut Context,
    key_handle: KeyHandle,
    public: &'a Key<PublicParts, UnspecifiedRole>,
}

impl openpgp::crypto::Signer for TpmKeyPair<'_> {
    fn public(&self) -> &Key<PublicParts, UnspecifiedRole> {
        self.public
    }

    fn sign(
        &mut self,
        hash_algo: HashAlgorithm,
        digest: &[u8],
    ) -> openpgp::Result<openpgp::crypto::mpi::Signature> {
        //eprintln!("hash_algo: {:?}", hash_algo);
        assert_eq!(hash_algo, HashAlgorithm::SHA256);
        let scheme = TPMT_SIG_SCHEME {
            scheme: TPM2_ALG_NULL,
            details: Default::default(),
        };
        let validation = TPMT_TK_HASHCHECK {
            tag: TPM2_ST_HASHCHECK,
            hierarchy: TPM2_RH_NULL,
            digest: Default::default(),
        }
        .try_into()?;

        let digest = TpmDigest::try_from(digest.to_vec())?;

        self.context.tr_set_auth(
            self.key_handle.into(),
            Auth::try_from(vec![0x01, 0x02, 0x07])?,
        )?;
        let signature = self
            .context
            .sign(self.key_handle, digest, scheme.try_into()?, validation)?;
        match signature {
            Signature::RsaSsa(sig) | Signature::RsaPss(sig) => {
                let sig = sig.signature().value().to_vec();
                Ok(openpgp::crypto::mpi::Signature::RSA { s: sig.into() })
            }
            _ => {
                Err(openpgp::Error::InvalidOperation(format!(
                    "Don't know how to handle non-RSA things."
                )).into())
            }
        }
    }
}
