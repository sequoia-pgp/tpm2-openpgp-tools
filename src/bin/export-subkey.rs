use openpgp::Error;
use openpgp::{
    packet::{
        key::{PublicParts, SubordinateRole},
        prelude::Key4,
        Key,
    },
    serialize::Marshal,
    Packet,
};
use sequoia_openpgp as openpgp;
use std::str::FromStr;
use std::{io, num::ParseIntError, time::SystemTime};
use structopt::StructOpt;
use tss_esapi::{
    attributes::SessionAttributesBuilder,
    constants::{
        session_type::SessionType,
    },
    handles::PersistentTpmHandle,
    interface_types::algorithm::HashingAlgorithm,
    structures::Public,
    structures::SymmetricDefinition,
    Context,
};
use tss_esapi::{
    handles::{KeyHandle, TpmHandle},
    Tcti,
};

fn parse_hex(src: &str) -> Result<u32, ParseIntError> {
    u32::from_str_radix(src, 16)
}

#[derive(StructOpt, Debug)]
#[structopt(name = "export-subkey")]
struct Opt {
    /// TCTI configuration. It can be used to run the program against TPM simulator.
    /// By default it uses hardware TPM. Depending on the device permissions it may require
    /// running with elevated permissions.
    #[structopt(short, long, default_value = "device:/dev/tpmrm0")]
    tcti: String,

    /// Handle for the persisted object, for example: 81000016.
    /// Use `tpm2_getcap handles-persistent` and use a value that is not present in the output.
    #[structopt(short, long, parse(try_from_str = parse_hex))]
    handle: u32,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let tcti = Tcti::from_str(&opt.tcti)?;

    let mut context = Context::new(tcti)?;

    let session = context.start_auth_session(
        None,
        None,
        None,
        SessionType::Hmac,
        SymmetricDefinition::AES_256_CFB,
        HashingAlgorithm::Sha256,
    )?;

    let (session_attrs, session_attrs_mask) = SessionAttributesBuilder::new()
        .with_encrypt(true)
        .build();
    context.tr_sess_set_attributes(
        session.unwrap(), session_attrs, session_attrs_mask)?;
    context.set_sessions((session, None, None));

    let persistent_tpm_handle = PersistentTpmHandle::new(opt.handle)?;

    let handle = context.execute_without_session(|ctx| {
        ctx.tr_from_tpm_public(TpmHandle::Persistent(persistent_tpm_handle))
            .expect("Need handle")
    });

    let key_handle: KeyHandle = handle.into();
    let (key_pub, _, _) = context.read_public(key_handle)?;

    if let Public::Rsa {
        object_attributes: _object_attributes,
        name_hashing_algorithm: _name_hashing_algorithm,
        auth_policy: _auth_policy,
        parameters: _parameters,
        unique
    } = key_pub {
        let key: Vec<u8> = unique.value().to_vec();
        // XXX(N): key.truncate(pub_key.size.try_into().unwrap()); // should not fail on supported targets

        let key4: Key<PublicParts, SubordinateRole> =
            Key4::import_public_rsa(&[1, 0, 1], &key, SystemTime::UNIX_EPOCH)?.into();

        let packet: Packet = key4.into();
        let mut stdout = io::stdout();
        packet.serialize(&mut stdout)?;

        Ok(())
    } else {
        Err(Error::InvalidOperation(format!("Don't know how to handle non-RSA things.")).into())
    }
}
