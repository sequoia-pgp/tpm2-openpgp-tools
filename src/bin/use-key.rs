use std::convert::TryInto;
use std::default::Default;
use std::str::FromStr;
use std::{convert::TryFrom, io::ErrorKind};
use tss_esapi::Tcti;
use tss_esapi::{
    attributes::SessionAttributesBuilder,
    constants::{
        session_type::SessionType,
    },
    handles::{KeyHandle, PersistentTpmHandle, TpmHandle},
    interface_types::algorithm::HashingAlgorithm,
    structures::{Auth, Data, Public, PublicKeyRsa, RsaDecryptionScheme, SymmetricDefinition},
    Context,
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("1");
    let tcti_cfg: Option<String> = Some("device:/dev/tpmrm0".into());
    //tcti.zeroize();

    let tcti = Tcti::from_str(tcti_cfg.as_ref().ok_or_else(|| {
        std::io::Error::new(ErrorKind::InvalidData, "TCTI configuration missing")
    })?)
    .map_err(|e| {
        println!("xx");
        std::io::Error::new(
            ErrorKind::InvalidData,
            format!("XInvalid TCTI configuration string: {:#?}", e),
        )
    })?;

    let mut context = Context::new(tcti)?;

    //context.

    let session = context.start_auth_session(
        None,
        None,
        None,
        SessionType::Hmac,
        SymmetricDefinition::AES_256_CFB,
        HashingAlgorithm::Sha256,
    )?;
    let (session_attrs, session_attrs_mask) = SessionAttributesBuilder::new()
        .with_decrypt(true)
        .with_encrypt(true)
        .build();
    context.tr_sess_set_attributes(
        session.unwrap(), session_attrs, session_attrs_mask)?;
    context.set_sessions((session, None, None));

    println!("2");
    //context.ev
    // println!("Vendor = {}", tss_esapi::utils::get_tpm_vendor(&mut context)?);

    // GENERATE AND PERSIST IN TPM
    let persistent_tpm_handle =
        PersistentTpmHandle::new(u32::from_be_bytes([0x81, 0x00, 0x00, 0x26]))?;

    println!("3");
    let handle = context.execute_without_session(|ctx| {
        ctx.tr_from_tpm_public(TpmHandle::Persistent(persistent_tpm_handle))
            .expect("Need handle")
    });

    println!("4");

    let msg: PublicKeyRsa = vec![0x01, 0x02, 0x03].try_into()?;

    let data = Data::default();

    println!("Data = {:?}", data);

    let key_handle: KeyHandle = handle.into();

    let cipher_text = context.rsa_encrypt(key_handle, msg, RsaDecryptionScheme::RsaEs, data.clone())?;

    println!("Cipher = {:X?}", cipher_text);
    context.tr_set_auth(handle.into(), Auth::try_from(vec![0x01, 0x02, 0x07])?)?;
    let decrypted = context.rsa_decrypt(key_handle, cipher_text, RsaDecryptionScheme::RsaEs, data)?;
    println!("Hello, world! Decrypted = {:X?}", decrypted);

    let (key_pub, name1, name2) = context.read_public(key_handle)?;

    println!("Pubkey = {:?}, {:?}", name1, name2);

    if let Public::Rsa {
        object_attributes: _object_attributes,
        name_hashing_algorithm: _name_hashing_algorithm,
        auth_policy: _auth_policy,
        parameters: _parameters,
        unique
    } = key_pub {
        let key: Vec<u8> = unique.value().to_vec();
        // XXX(N): key.truncate(pub_key.size.try_into().unwrap()); // should not fail on supported targets

        println!("{:x?}", &key);
    }

    Ok(())
}
