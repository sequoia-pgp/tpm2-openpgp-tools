use std::str::FromStr;
use std::{convert::TryFrom, convert::TryInto, fs::File, num::ParseIntError, path::PathBuf};
use structopt::StructOpt;
use tss_esapi::{
    attributes::SessionAttributesBuilder,
    attributes::object::ObjectAttributesBuilder,
    constants::{
        session_type::SessionType,
    },
    handles::PersistentTpmHandle,
    interface_types::{
        algorithm::HashingAlgorithm,
        algorithm::PublicAlgorithm,
        dynamic_handles::Persistent,
        key_bits::RsaKeyBits,
        resource_handles::{Hierarchy, Provision},
    },
    structures::{
        Auth,
        HashScheme,
        Public,
        PublicBuilder,
        PublicKeyRsa,
        PublicRsaParametersBuilder,
        RsaExponent,
        RsaScheme,
        SymmetricDefinition,
    },
    Context, Tcti,
};

fn parse_hex(src: &str) -> Result<u32, ParseIntError> {
    u32::from_str_radix(src, 16)
}

#[derive(StructOpt, Debug)]
#[structopt(name = "gen-signing-key")]
struct Opt {
    /// TCTI configuration. It can be used to run the program against TPM simulator.
    /// By default it uses hardware TPM. Depending on the device permissions it may require
    /// running with elevated permissions.
    #[structopt(short, long, default_value = "device:/dev/tpmrm0")]
    tcti: String,

    /// Authentication file. It's like a PIN for accessing the key. You can use `head -c 3 /dev/urandom`
    /// to generate this file. Do not lose it as it's required to use the key!
    #[structopt(short, long, parse(from_os_str))]
    auth_file: PathBuf,

    /// Handle for the persisted object, for example: 81000016.
    /// Use `tpm2_getcap handles-persistent` and use a value that is not present in the output.
    #[structopt(short, long, parse(try_from_str = parse_hex))]
    handle: u32,
}

pub fn create_unrestricted_signing_rsa_public(
    scheme: RsaScheme,
    key_bits: RsaKeyBits,
    pub_exponent: RsaExponent,
) -> tss_esapi::Result<Public> {
    let object_attributes = ObjectAttributesBuilder::new()
        .with_fixed_tpm(true)
        .with_fixed_parent(true)
        .with_sensitive_data_origin(true)
        .with_user_with_auth(true)
        .with_decrypt(false)
        .with_sign_encrypt(true)
        .with_restricted(false)
        .build()?;

    PublicBuilder::new()
        .with_public_algorithm(PublicAlgorithm::Rsa)
        .with_name_hashing_algorithm(HashingAlgorithm::Sha256)
        // XXX(N): From v4 code:
        // PublicIdUnion::Rsa(Box::new(Default::default())).  Check.
        // (Perhaps its not needed at all.)
        .with_rsa_unique_identifier(
            PublicKeyRsa::new_empty_with_size(key_bits))
        .with_rsa_parameters(
            PublicRsaParametersBuilder::new_unrestricted_signing_key(
                scheme, key_bits, pub_exponent)
                .build()?)
        .with_object_attributes(object_attributes)
        .build()
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let tcti = Tcti::from_str(&opt.tcti)?;

    let mut context = Context::new(tcti)?;

    let session = context.start_auth_session(
        None,
        None,
        None,
        SessionType::Hmac,
        SymmetricDefinition::AES_256_CFB,
        HashingAlgorithm::Sha256,
    )?;

    let (session_attrs, session_attrs_mask) = SessionAttributesBuilder::new()
        .build();
    context.tr_sess_set_attributes(
        session.unwrap(), session_attrs, session_attrs_mask)?;
    context.set_sessions((session, None, None));

    let key_auth = {
        use std::io::prelude::*;
        let mut f = File::open(opt.auth_file)?;
        let mut v = Vec::new();
        f.read(&mut v)?;
        Auth::try_from(v)?
    };
    let pk = context.create_primary(
        Hierarchy::Owner,
        create_unrestricted_signing_rsa_public(
            RsaScheme::RsaSsa(HashScheme::new(HashingAlgorithm::Sha256)),
            RsaKeyBits::Rsa2048, RsaExponent::ZERO_EXPONENT,
        )?.try_into()?,
        Some(key_auth),
        None,
        None,
        None,
    )?;

    println!("Generated key handle = 0x{:X}", pk.key_handle.value());

    let persistent = Persistent::Persistent(PersistentTpmHandle::new(opt.handle)?);

    context.evict_control(Provision::Owner, pk.key_handle.into(), persistent)?;

    println!("Key persisted. Check `tpm2_getcap handles-persistent`.");
    println!(
        "To remove the key from the TPM use `tpm2_evictcontrol -c 0x{:X}`.",
        opt.handle
    );

    Ok(())
}
