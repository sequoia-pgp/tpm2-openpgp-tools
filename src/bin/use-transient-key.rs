use std::convert::TryInto;
use std::io::ErrorKind;
use std::str::FromStr;
use tss_esapi::Tcti;
use tss_esapi::{
    abstraction::transient::KeyParams,
    interface_types::key_bits::RsaKeyBits,
    structures::PublicKeyRsa,
    structures::RsaExponent,
    structures::RsaScheme,
};
const AUTH_VAL_LEN: usize = 32;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let tcti_cfg: Option<String> = Some("device:/dev/tpmrm0".into());
    //tcti.zeroize();

    let tcti = Tcti::from_str(tcti_cfg.as_ref().ok_or_else(|| {
        std::io::Error::new(ErrorKind::InvalidData, "TCTI configuration missing")
    })?)
    .map_err(|e| {
        println!("xx");
        std::io::Error::new(
            ErrorKind::InvalidData,
            format!("XInvalid TCTI configuration string: {:#?}", e),
        )
    })?;

    let mut esapi_context =
        tss_esapi::abstraction::transient::TransientKeyContextBuilder::new()
            .with_tcti(tcti)
            //.with_root_key_size(ROOT_KEY_SIZE)
            //.with_root_key_auth_size(ROOT_KEY_AUTH_SIZE)
            //.with_hierarchy_auth(get_hierarchy_auth(&mut owner_hierarchy_auth)?)
            //.with_hierarchy(tss_esapi::interface_types::resource_handles::Hierarchy::Owner)
            //.with_session_hash_alg(HashingAlgorithm::Sha256)
            //.with_default_context_cipher(find_default_context_cipher(tcti_cfg)?)
            .build()
            .map_err(|e| {
                eprintln!("Error creating TSS Transient Object Context: {}", e);
                std::io::Error::new(ErrorKind::InvalidData, "failed initializing TSS context")
            })?;

    println!("Context ready");

    //let mut esapi_context = esapi_context
    //.lock()
    //.expect("ESAPI Context lock poisoned");

    let key_params = KeyParams::Rsa {
        size: RsaKeyBits::Rsa2048,
        // XXX(N): Unsure.
        scheme: RsaScheme::RsaEs,
        pub_exponent: RsaExponent::ZERO_EXPONENT,
    };
    let (key_context, auth_value) = esapi_context.create_key(
        key_params.clone(),
        AUTH_VAL_LEN,
    )?;

    println!("Key ready: Auth = {:?}", auth_value);

    //let key = esapi_context.read_public_key(key_context)?;
    //println!("Public Key = {:?}", key);

    //if let PublicKey::Rsa(bytes) = key {

    //let id = key::Id::from_persistent_key_id(key_id);
    //let key_attributes = key::Attributes::from_key_id(id)?;

    //let rsa = esapi_context.load_external_rsa_public_key(&bytes)?;

    let msg: PublicKeyRsa = vec![0x01, 0x02, 0x03].try_into()?;

    println!("msg ready");

    //println!("Public Key Loaded = {:?}", rsa);
    let result = esapi_context.rsa_encrypt(
        key_context.clone(),
        key_params,
        auth_value.clone(),
        msg,
        None,
    )?;
    println!("Hello, world! {:X?}", result);
    let decrypted = esapi_context.rsa_decrypt(
        key_context,
        key_params.clone(),
        auth_value,
        result,
        None,
    )?;
    println!("Hello, world! Decrypted = {:X?}", decrypted);
    //} else {
    //    eprintln!("Expected RSA");
    //}

    Ok(())
}
