# OpenPGP TPM2 tools

This suite of tools exposes TPM2 cryptography (hardware-backed keys, decryption and signing) in OpenPGP format through Sequoia PGP.

## Usage

Do mind `#` prompts: commands prepended with `#` need to be run with elevated permissions (e.g.`sudo`; for accessing `/dev/tpmrm0` device). Setting up appropriate permissions for that device file should allow other uses to run the TPM2-touching binaries.

On certain distros the `/dev/tpmrm0` device allows access for users in `tss` group so the safer alternative would be to add the calling user to the `tss` group:

    # usermod -a -G tss $USER

Note that your effective groups will be changed after re-login, use `groups` to see the current values.

### Creating primary key

In production it is very likely that you already have a trusted primary key. You can and probably *should* continue using it and just append TPM2-backed signing keys as subkeys.

The instructions below are just an example mostly for testing.

Create primary key:

    $ sq key generate --cannot-encrypt --cannot-sign --userid "<test-tpm@metacode.biz>" --export primary.sec.asc

Extract public parts from the key:

    $ sq key extract-cert < primary.sec.asc > primary.pub.asc

### Creating TPM-backed signing key

Move `primary.pub.asc` to the online machine.

Create an auth file (like PIN):

    $ head -c 3 /dev/urandom > auth2

Generate the signing key in the TPM:

    # gen-signing-key -a auth2 -h 81000017

Export OpenPGP subkey packet for the key:

    # export-subkey -h 81000017 > subkey.pub.pgp

Create backsig for the primary key:

    # create-backsig -a auth2 -h 81000017 < primary.pub.asc > backsig.pgp

Move `subkey.pub.pgp` and `backsig.pgp` to the offline machine.

Bind subkey to the primary key:

    $ bind-subkey --backsig backsig.pgp --subkey subkey.pub.pgp < primary.sec.asc > subkey-binding.pgp

Now, to create a complete key, convert primary key to binary and concatenate it with the subkey public key and the binding signature:

    $ sq dearmor < primary.pub.asc > primary.pub.pgp
    $ cat primary.pub.pgp subkey.pub.pgp subkey-binding.pgp > complete-key.pgp

`complete-key.pgp` can be uploaded to keyserver, shared, imported and used as usual.

You can complete this section as many times as needed if multiple machines need signing keys or if for some reason you need multiple signing keys.

### Signing data

First let's assume we have a file to be signed:

    $ echo dummy test > file.txt

Then we sign it using the TPM-backed key:

    # detach-sign -a auth2 -h 81000017 < file.txt  > file.txt.sig

And that's it. There is no step 3.

The signature can be later verified with other OpenPGP tools:

```
$ gpg --verify file.txt.sig
gpg: assuming signed data in 'file.txt'
gpg: Signature made Wed, 17 Feb 2021, 13:13:41 CET
gpg:                using RSA key 94FD197A5FDC49B810963C27750AEDE82D5C7CF7
gpg: Good signature from "<test-tpm@metacode.biz>" [unknown]
```

## Caveats

Tools that are built but do not have instructions described here are just experiments and should not be relied upon.

## Known issues

- Currently only RSA 2048 keys are generated and supported.
- Generated keys have key creation time set to unix epoch.
